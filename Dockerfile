FROM ubuntu:14.04.1

# Add files.
ADD bin/rabbitmq-start /usr/local/bin/

RUN apt-get update
RUN apt-get install -y postgresql-9.3 wget vim

# Install RabbitMQ.
# Define environment variables.



USER postgres
RUN echo "host\tall\t\tall\t\t0.0.0.0/0\t\tmd5" >> /etc/postgresql/9.3/main/pg_hba.conf
RUN echo "listen_addresses='*'" >> /etc/postgresql/9.3/main/postgresql.conf
RUN /etc/init.d/postgresql start &&\
    psql --command "CREATE USER docker WITH SUPERUSER PASSWORD 'docker';" &&\
    createdb -O docker docker
CMD ["/usr/lib/postgresql/9.3/bin/postgres", "--config-file=/etc/postgresql/9.3/main/postgresql.conf"]

# Expose ports.
EXPOSE 5432
EXPOSE 5672
EXPOSE 15672
